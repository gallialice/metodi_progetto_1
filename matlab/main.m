clear all, close all, clc;

% Caricamento matrici dalla cartella "matrixes".
matrices = dir('Matrices\*.mat');

% Creazione delle variabili in cui salvare i risultati.
% table_variables = {'name', 'time (s)', 'relative error', ...
%     'initial memory usage (MB)', 'condition number';};



% Caricamento della singola matrice A.
matrix = load(strcat(pwd,'/matrices/ex15.mat'));
% matrix = load(strcat(pwd,'/matrices/shallow_water1.mat'));
% matrix = load(strcat(pwd,'/matrices/cfd1.mat'));
% matrix = load(strcat(pwd,'/matrices/cfd2.mat'));
% matrix = load(strcat(pwd,'/matrices/parabolic_fem.mat'));
% matrix = load(strcat(pwd,'/matrices/apache2.mat'));
% matrix = load(strcat(pwd,'/matrices/G3_circuit.mat'));
% matrix = load(strcat(pwd,'/matrices/StocF-1465.mat'));
% matrix = load(strcat(pwd,'/matrices/Flan_1565.mat'));

disp("Matrix loaded!");

tmp = split(matrix.Problem.name, "/");
matrix_name = tmp(2);

myChol(matrix, matrix_name);

function myChol(matrix, matrix_name)
    A = matrix.Problem.A;
    
    
    % Scrittura header file csv
    file_name = strcat(pwd, "/csv/", matrix_name,'.csv');
    file = fopen(file_name(1),'w');
    fprintf(file, strcat("name",","));
    fprintf(file, strcat("time (s)",","));
    fprintf(file, strcat("error",","));
    fprintf(file, strcat("memory_used (MB)",","));
    fprintf(file, strcat("number of conditioning","\n"));
    fclose(file);

    % Stampa a schermo del nome della matrice.
    disp(matrix.Problem.name);
   
    % Calcolo del numero di condizionamento della matrice.
    k = condest(A);
    
    % Impostazione della soluzione esatta formata solo da 1.
    xe = ones(length(A),1);

    % Calcolo della memoria occupata prima della risoluzione del sistema
    % lineare.
    allvars = whos;
    memory_start = sum([allvars.bytes]);
    % memory_start = memory;

    % Inizio del calcolo del tempo di risoluzione del sistema lineare.
    start_time = tic;
    
    % Chol decomposition
    % R = chol(A);
    [R, p, S] = chol(A);

    disp("Cholesky decomposition executed!");
    
    if p ~= 0
        fprintf("Matrix non definite positive!");
    end
    
    
    % Calcolo del vettore dei termini noti a partire dalla matrice A e
    % dalla soluzione esatta.
    b = A*xe;
    
    % Abilitazione della stampa a schermo di informazioni dettagliate
    % sulla risoluzione di sistemi lineari per matrici sparse.
    % spparms('spumoni', 2);
    
    % Risoluzione del sistema lineare con metodo di Cholesky.
    % x = A\b;
    
    x = S*(R\(R'\(S'*b)));

    disp("Linear system solved!");
    
    % Termine del calcolo del tempo di risoluzione del sistema lineare.
    time = toc(start_time);

    allvars = whos;
    memory_end = sum([allvars.bytes]);
    

    memory_used = (memory_end - memory_start)/10^6;
    
    % Calcolo dell'errore relativo (come norma Euclidea) fra la soluzione 
    % calcolata e la soluzione esatta.
    error = norm(x-xe,2)/norm(xe,2);
    
    file = fopen(file_name(1),'a');
    fprintf(file, strcat(matrix_name,","));
    fprintf(file, strcat(string(time),","));
    fprintf(file, strcat(string(error),","));
    fprintf(file, strcat(string(memory_used),","));
    fprintf(file, strcat(string(k),"\n"));
    fclose(file);

    disp("File .csv written!");

    % Rimozione delle variabili temporanee allocate. 
    clear memory_start;
    clear k, clear xe, clear b, clear x, clear time, clear error;
end